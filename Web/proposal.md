# CSCI S-33a: Project Proposal
## Due: Friday, July 27 at 6pm

#### Your name

TODO

#### Your teaching fellow's name

TODO

#### Which language(s) will you use for your project?

TODO

#### What will (likely) be the title of your project?

TODO

#### In just a sentence or two, summarize your project. (e.g. "A website that lets you check the weather in different cities.")

TODO

#### Where will your project ultimately live? (e.g. within CS50 IDE, Heroku, AWS, some commerical web host...)

TODO

#### In a paragraph or more, detail your project. What will it do? What features will it have?

TODO

<hr>

- In the world of software, most everything takes longer to implement than you expect. And so it's not uncommon to accomplish less in a fixed amount of time than you hope.

#### In a sentence or list, define a GOOD outcome for your project. What WILL you accomplish no matter what?

TODO

#### In a sentence or list, define a BETTER outcome for your project. What do you THINK you will accomplish in time?

TODO

#### In a sentence or list, define a BEST outcome for your project. What do you HOPE you will accomplish in time?

TODO

#### In a paragraph or more, outline your next steps. What new skills do you need to acquire? What topics will you need to research?

TODO
